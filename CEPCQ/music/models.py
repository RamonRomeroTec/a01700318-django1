from django.db import models

class Album (models.Model):
    artista = models.CharField(max_length=250)
    titulo_album = models.CharField(max_length=250)
    genero = models.CharField(max_length=250)
    imagen = models.CharField(max_length=250)

    def __str__(self):
        return self.titulo_album + ' - ' + self.artista

class Song (models.Model):
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    tipo = models.CharField(max_length=250)
    titulocancion = models.CharField(max_length=250)
    isFav = models.BooleanField(default=False)

    def __str__(self):
        return self.titulocancion
