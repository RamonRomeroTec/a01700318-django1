from django.http import HttpResponse
from django.http import Http404
from .models import Album, Song
from django.template import loader
from django.shortcuts import render, get_object_or_404


def index(request):
    all_albums = Album.objects.all()
    '''
    html=''
    for album in all_albums:
        url='/music/'+ str(album.id) + '/'
        html += '<a href="'+url+'">'+ album.titulo_album + ' </a><br>'
    return HttpResponse(html)
    '''

    context = {'all_albums': all_albums}
    return render(request, 'music/index.html', context)


def detail(request, album_id):
    '''
    try:
        album = Album.objects.get(pk=album_id)
    except Album.DoesNotExist:
        raise Http404("Album no existe")
    return render(request, "music/detail.html", {'album': album})
    '''

    album = get_object_or_404(Album, pk=album_id)
    return render(request, "music/detail.html", {'album': album})


def fav(request, album_id):
    '''
    try:
        album = Album.objects.get(pk=album_id)
    except Album.DoesNotExist:
        raise Http404("Album no existe")
    return render(request, "music/detail.html", {'album': album})
    '''

    album = get_object_or_404(Album, pk=album_id)
    try:
        selected= album.song_set.get(pk=request.POST['song'])
    except(KeyError, Song.DoesNotExist):
        return render(request, "music/detail.html", {'album': album, 'error': "fail"})
    else:
        selected.isFav=True
        selected.save()
        return render(request, "music/detail.html", {'album': album})




